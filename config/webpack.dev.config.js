const path = require("path");

module.exports = {
  mode: "development",
  watch: true,
  entry: "./src/index.js",
  output: {
    path: path.join(__dirname, "../dist"),
    filename: "bundle.js",
    publicPath: "http://localhost:3030/dist/"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [/node_modules/],
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.(png|jpg)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[path][name].[ext]"
            }
          }
        ]
      }
    ]
  },
  devServer: {
    hot: true,
    inline: true,
    open: true,
    noInfo: false,
    host: "0.0.0.0",
    port: 3030,
    contentBase: "./src"
  }
};
