import imageStep1 from "../assets/images/step1.jpg";
import imageStep2 from "../assets/images/step2.jpg";
import imageStep3 from "../assets/images/step3.jpg";

export const aboutData = [
  {
    title: "Sed leo enim, condimentum",
    text:
      "Quisque libero libero, dictum non turpis in, luctus semper lorem. Donec rhoncus a leo sit amet facilisis.",
    stepNumber: "1",
    image: imageStep1
  },
  {
    title: "Morbi velit risus",
    text:
      "Nulla venenatis tempor dui in molestie. Nulla quis dictum purus, sit amet porttitor est.",
    stepNumber: "2",
    image: imageStep2
  },
  {
    title: "Sed leo enim, condimentum",
    text:
      "Quisque libero libero, dictum non turpis in, luctus semper lorem. Donec rhoncus a leo sit amet facilisis.",
    stepNumber: "3",
    image: imageStep3
  }
];
