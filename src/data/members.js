import marieBennett from "../assets/images/marie-bennett.jpg";
import bradleyHunter from "../assets/images/bradley-hunter.jpg";
import dianaWells from "../assets/images/diana-wells.jpg";
import christopherPierce from "../assets/images/christopher-pierce.jpg";

export const membersData = [
  {
    image: bradleyHunter,
    icon: "",
    name: "Bradley Hunter",
    miniBio: "Based in Chicago. I love playing tennis and loud music."
  },
  {
    image: dianaWells,
    icon: "",
    name: "Diana Wells",
    miniBio:
      "Living in Athens, Greece. I love black and white classics, chillout music and green tea."
  },
  {
    image: marieBennett,
    icon: "",
    name: "Marie Bennett",
    miniBio:
      "Currently living in Colorado. Lover of art, languages and travelling."
  },
  {
    image: christopherPierce,
    icon: "",
    name: "Christopher Pierce",
    miniBio:
      "Star Wars fanatic. I have a persistent enthusiasm to create new things. "
  }
];
