import React from "react";
import { css } from "glamor";
import Button from "../../components/Button";
import { membersData } from "../../data/members";
import { $brownishGrey, $greyishBrown } from "../../styles/colors";
import { robotoFont, small, monteserratFont, title } from "../../styles/fonts";

const fontStyle = {
  fontStyle: "normal",
  fontStretch: "normal",
  letterSpacing: "normal",
  lineHeight: "1",
  textAlign: "center",
  display: "block"
};

const styles = {
  title: css({
    color: $greyishBrown,
    ...title
  }),
  name: css({
    fontFamily: monteserratFont,
    fontSize: small,
    fontWeight: "bold",
    color: $greyishBrown,
    margin: "10px",
    ...fontStyle
  }),
  miniBio: css({
    color: $brownishGrey,
    fontFamily: robotoFont,
    fontSize: small,
    fontWeight: "300",
    ...fontStyle,
    maxWidth: "90%",
    "@media(max-width: 321px)": {
      maxWidth: "100%"
    }
  }),
  image: css({
    margin: "0 auto",
    display: "block"
  }),
  listMemberItem: css({
    listStyle: "none",
    "@media(min-width: 321px)": {
      width: "45%"
    },
    "@media(min-width: 1024px)": {
      width: "25%"
    }
  }),
  listMembers: css({
    margin: "0",
    padding: "0",
    "@media(min-width: 1024px)": {
      display: "inline-flex",
      flexWrap: "noWrap"
    },
    "@media(min-width: 321px)": {
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center"
    }
  }),
  button: css({
    display: "block",
    margin: "30px auto 0 auto"
  })
};

const Members = () => {
  return (
    <div>
      <h2 {...styles.title}>Meet a partner for your best holiday</h2>
      <ul {...styles.listMembers}>
        {membersData.map((member, index) => (
          <li key={`members-${index}`} {...styles.listMemberItem}>
            <img {...styles.image} src={member.image} />
            <span {...styles.name}>{member.name}</span>
            <span {...styles.miniBio}>{member.miniBio}</span>
          </li>
        ))}
      </ul>
      <Button
        style={styles.button}
        text="See other partners"
        isPrimary={false}
      />
    </div>
  );
};

export default Members;
