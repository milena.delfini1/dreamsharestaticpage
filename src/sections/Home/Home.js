import React, { Component } from "react";
import { css } from "glamor";
import Button from "../../components/Button";
import { normalStyleFont, monteserratFont, small } from "../../styles/fonts";
import { $white, $veryLightPink } from "src/styles/colors";
import { homeData } from "../../data/home";
const styles = {
  wrapperBackground: css({
    backgroundImage:
      'linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, -0.9)),url("../../assets/images/home.jpg")',
    height: "540px",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    objectFit: "contain",
    opacity: "0.8",
    padding: "23px",
    color: $white,
    "@media(min-width: 767px)": {
      backgroundImage:
        'linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, -0.9)),url("../../assets/images/home-desk.jpg")'
    }
  }),
  title: css({
    fontFamily: monteserratFont,
    fontSize: "30px",
    fontWeight: "bold",
    textAlign: "center",
    textTransform: "uppercase",
    whiteSpace: "pre",
    ...normalStyleFont,
    "@media(min-width: 767px)": {
      fontSize: "50px"
    }
  }),
  subtitle: css({
    textAlign: "center",
    fontSize: "24px",
    color: $veryLightPink
  }),
  wrapperContent: css({
    display: "flex",
    flexDirection: "column",
    position: "relative",
    top: "20%"
  }),
  mainButton: css({
    width: "70%",
    maxWidth: "225px",
    margin: "0 auto",
    "@media(max-width: 320px)": {
      width: "100%",
      maxWidth: "unset",
      margin: "unset"
    }
  }),
  brand: css({
    textTransform: "uppercase",
    textAlign: "center",
    fontWeight: "bold",
    fontFamily: monteserratFont,
    color: $white,
    fontSize: "20px"
  }),
  signUpButton: css({
    color: $white,
    border: `2px solid ${$white}`,
    fontFamily: monteserratFont,
    fontSize: small
  }),
  logInLink: css({
    color: $white,
    fontFamily: monteserratFont,
    fontSize: small,
    marginRight: "20px",
    cursor: "pointer"
  }),
  actionHeader: css({
    display: "flex",
    justifyContent: "space-around"
  })
};
class Home extends Component {
  render() {
    return (
      <div {...styles.wrapperBackground}>
        <div {...styles.actionHeader}>
          <div {...styles.brand}>DreamShare</div>
          <div {...styles.headerActions}>
            <a {...styles.logInLink}>Log In</a>
            <Button
              text="Sign Up"
              isPrimary={false}
              style={styles.signUpButton}
            />
          </div>
        </div>
        <div {...styles.wrapperContent}>
          <h1 {...styles.title}>{homeData.mainTitle}</h1>
          <p {...styles.subtitle}>{homeData.subtitle}</p>
          <Button style={styles.mainButton} text={homeData.mainButton} />
        </div>
      </div>
    );
  }
}
export default Home;
