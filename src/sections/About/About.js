import React from "react";
import { css } from "glamor";
import { $greyishBrown } from "styles/colors";
import { title } from "styles/fonts";
import { aboutData } from "data/about";
import StepCard from "components/StepCard";

const styles = {
  title: css({
    color: $greyishBrown,
    ...title
  }),
  aboutList: css({
    listStyle: "none",
    display: "flex",
    justifyContent: "center",
    height: "390px"
  }),
  listItem: css({
    width: "30%",
    height: "100%",
    marginRight: "5px",
    "@media(max-width: 321px)": {
      width: "100%"
    }
  }),
  wrapperList: css({
    margin: "0 auto",
    width: "80%",
    "@media(max-width: 321px)": {
      width: "100%"
    }
  })
};
const About = () => (
  <div>
    <h2 {...styles.title}>How Dreamshare</h2>
    <div {...styles.wrapperList}>
      <ul {...styles.aboutList}>
        {aboutData.map((item, index) => (
          <li key={`about-${index}`} {...styles.listItem}>
            <StepCard
              title={item.title}
              text={item.text}
              step={item.stepNumber}
              image={item.image}
            />
          </li>
        ))}
      </ul>
    </div>
  </div>
);

export default About;
