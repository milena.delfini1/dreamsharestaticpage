import React from "react";
import { css } from "glamor";
import aretoDevelopmentImage from "src/assets/images/areto-development.png";
import { $lightRed, $brownGrey } from "src/styles/colors";
import { monteserratFont } from "styles/fonts";
import { $veryLightPinkTwo } from "styles/colors";

const styles = {
  wrapperFooter: css({
    display: "flex",
    flexFlow: "row",
    borderTop: `3px solid ${$lightRed}`,
    justifyContent: "space-around",
    "@media(max-width: 767px)": {
      flexFlow: "column",
      alignItems: "center"
    }
  }),
  section: css({
    margin: "20px 0px"
  }),
  subjectList: css({
    listStyle: "none",
    lineHeight: "1.86",
    margin: "0",
    padding: "0",
    display: "flex",
    flexWrap: "wrap",
    color: $brownGrey,
    fontFamily: "Roboto",
    fontSize: "14px",
    fontWeight: "300",
    fontStyle: "normal",
    fontStretch: "normal",
    letterSpacing: "normal",
    "@media(max-width: 767px)": {
      display: "flex",
      justifyContent: "center"
    }
  }),
  subjectItem: css({
    width: "50%",
    "@media(max-width: 767px)": {
      display: "inline-block",
      marginLeft: "10px",
      width: "unset"
    }
  }),
  brandName: css({
    width: "119px",
    height: "19px",
    fontSize: "16px",
    color: $lightRed,
    textTransform: "uppercase"
  }),
  developerLogo: css({
    width: "110px",
    height: "24px",
    objectFit: "contain"
  }),
  companyTitle: css({
    fontWeight: "bold",
    margin: "0",
    "@media(max-width: 767px)": {
      textAlign: "center"
    }
  }),
  designedByLabel: css({
    fontFamily: monteserratFont,
    fontSize: "10px",
    fontWeight: "bold",
    fontStyle: "normal",
    fontStretch: "normal",
    letterSpacing: "1px",
    display: "block",
    textTransform: "uppercase",
    textAlign: "right",
    color: $veryLightPinkTwo,
    "@media(max-width: 767px)": {
      textAlign: "center"
    }
  })
};

const Footer = () => {
  const subjects = [
    { text: "About", path: "/about" },
    { text: "Contact", path: "/contact" },
    { text: "Press", path: "/press" },
    { text: "Blog", path: "/blog" },
    { text: "Terms and Privacy", path: "/terms-and-privacy" },
    { text: "Help", path: "/help" }
  ];
  return (
    <div {...styles.wrapperFooter}>
      <div {...styles.section}>
        <span {...styles.brandName}>Dreamshare</span>
      </div>
      <div {...styles.section}>
        <p {...styles.companyTitle}>Company</p>
        <ul {...styles.subjectList}>
          {subjects.map((subject, index) => (
            <li key={`subject-${index}`} {...styles.subjectItem}>
              {subject.text}
            </li>
          ))}
        </ul>
      </div>
      <div {...styles.section}>
        <span {...styles.designedByLabel}>Designed By</span>
        <img
          {...styles.developerLogo}
          src={aretoDevelopmentImage}
          alt="logotipo areto development"
        />
      </div>
    </div>
  );
};

export default Footer;
