import React from "react";
import Button from "../../components/Button";
import Input from "../../components/Input/Input";
import { css } from "glamor";
import { robotoFont, medium, title } from "../../styles/fonts";
import { $brownishGrey, $greyishBrown } from "../../styles/colors";

const styles = {
  title: css({
    color: $greyishBrown,
    ...title
  }),
  label: css({
    fontFamily: robotoFont,
    fontSize: medium,
    display: " block",
    marginBottom: "20px",
    textAlign: "center",
    color: $brownishGrey
  }),
  wrapperSearch: css({
    padding: "20px",
    display: "flex",
    alignItems: "baseline",
    justifyContent: "center",
    marginBottom: "20px",
    "@media(max-width: 321px)": {
      flexFlow: "column",
      alignItems: "center"
    }
  }),
  searchInput: css({
    marginRight: "10px",
    width: "40%",
    "@media(max-width: 321px)": {
      width: "100%",
      marginRight: "0"
    }
  }),
  searchButton: css({
    maxWidth: "172px",
    minWidth: "126px",
    width: "30%",
    "@media(max-width: 321px)": {
      width: "100%",
      maxWidth: "unset"
    }
  })
};

const Search = () => (
  <div {...styles.wrapperSearchSection}>
    <h2 {...styles.title}>Create your holiday activity</h2>
    <label {...styles.label} htmlFor="interest-field">
      Hi! What are your holiday interests?
    </label>
    <div {...styles.wrapperSearch}>
      <Input
        style={styles.searchInput}
        id={"interest-field"}
        placeholder="Enter your interests"
      />
      <Button style={styles.searchButton} text="Search partners" />
    </div>
  </div>
);

export default Search;
