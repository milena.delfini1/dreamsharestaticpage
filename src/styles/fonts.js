export const small = "14px";
export const medium = "18px";
export const large = "22px";

export const monteserratFont = "Montserrat";
export const robotoFont = "Roboto";

export const normalStyleFont = {
  fontStyle: "normal",
  fontStretch: "normal",
  lineHeight: "normal",
  letterSpacing: "normal"
};

export const contentText = {
  fontFamily: "Roboto",
  fontSize: medium
};

export const title = {
  fontFamily: monteserratFont,
  fontSize: large,
  fontWeight: "bold",
  marginBottom: "43px",
  marginTop: "95px",
  textAlign: "center",
  normalStyleFont
};
