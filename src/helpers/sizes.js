export const isMobile = ({ width }) => width < 320;
export const isTablet = ({ width }) => width >= 321 && width < 1024;
export const isDesktop = ({ width }) => width >= 1024;
