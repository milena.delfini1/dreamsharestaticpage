import React from "react";
import { string } from "prop-types";
import { css } from "glamor";
import { monteserratFont, large } from "styles/fonts";
import { $white } from "styles/colors";
import Button from "../Button";

const styles = {
  title: css({
    fontStyle: monteserratFont,
    fontSize: large,
    color: $white
  }),
  card: image =>
    css({
      backgroundImage: `linear-gradient(bottom, rgba(0.3, 0, 0, 0.3), rgba(0, 0, 0, -0.9)), url(${image})`,
      backgroundRepeat: "no-repeat",
      height: "100%",
      display: "flex",
      flexDirection: "column",
      alignItems: "baseline",
      justifyContent: "flex-end",
      padding: "20px",
      boxSizing: "border-box"
    }),
  text: css({
    color: $white,
    width: "80%"
  }),
  stepButton: css({
    padding: "5px 10px"
  })
};

const StepCard = props => (
  <div {...styles.card(props.image)}>
    <Button style={styles.stepButton} text={`Step ${props.step}`} />
    <h3 {...styles.title}>{props.title}</h3>
    <p {...styles.text}>{props.text}</p>
  </div>
);

StepCard.propTypes = {
  title: string,
  text: string,
  step: string,
  image: string
};
export default StepCard;
