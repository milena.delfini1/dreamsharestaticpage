import React from "react";
import { string, func, object } from "prop-types";
import { css } from "glamor";
import { $lightgray } from "../../styles/colors";

const styles = {
  input: css({
    height: "30px",
    borderRadius: "3px",
    boxShadow: "0 1px 2px 0 rgba(0, 0, 0, 0.07)",
    background: "none",
    padding: "5px",
    marginBottom: "10px",
    border: `1px solid ${$lightgray}`
  })
};
const Input = props => {
  return (
    <input
      {...css(styles.input, props.style)}
      id={props.id}
      aria-label={props.ariaLabel}
      placeholder={props.placeholder}
      onChange={props.onChange}
    />
  );
};

Input.propTypes = {
  id: string,
  ariaLabel: string,
  placeholder: string,
  style: object,
  onChange: func
};
export default Input;
