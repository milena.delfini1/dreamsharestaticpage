import React from "react";
import { bool, func, object, string } from "prop-types";
import { css } from "glamor";
import { $lightRed, $white } from "src/styles/colors";

const styles = {
  button: isPrimary =>
    css({
      borderRadius: "40px",
      padding: "10px",
      background: isPrimary ? $lightRed : "none",
      border: `2px solid ${$lightRed}`,
      color: isPrimary ? $white : $lightRed,
      fontWeight: "bold",
      cursor: "pointer"
    })
};
const Button = props => {
  return (
    <button
      {...css(styles.button(props.isPrimary), props.style)}
      aria-label={props.ariaLabel}
      onClick={props.onClick}
    >
      {props.text}
    </button>
  );
};

Button.propTypes = {
  onClick: func.isRequired,
  isPrimary: bool.isRequired,
  text: string,
  ariaLabel: string,
  style: object
};

Button.defaultProps = {
  onClick: () => {},
  isPrimary: true
};

export default Button;
