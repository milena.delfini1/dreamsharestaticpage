import React from "react";
import Footer from "sections/Footer";
import Search from "sections/Search";
import Members from "sections/Members";
import About from "sections/About";
import Home from "sections/Home";

const App = () => (
  <React.Fragment>
    <Home />
    <About />
    <Members />
    <Search />
    <Footer />
  </React.Fragment>
);

export default App;
